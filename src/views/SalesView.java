package views;

import controllers.NameOfGoods;
import models.Product;
import utils.Validator;

import java.util.Arrays;
import java.util.Scanner;

public class SalesView {

    private String title;
    private String name;
    private int quantity;
    private double price;
    private Scanner scanner;
    private Product model;

    public SalesView(Product model) {
        this.model = model;
    }

    public void getInputs() {

        scanner = new Scanner(System.in);

        title = "Please, input name of goods, choose it from this list: ";
        System.out.println(title);
        System.out.println(Arrays.toString(NameOfGoods.values()));
        name = Validator.validateName(scanner);
        model.setName(name);

        title = "Please, input quantity of goods: ";
        System.out.println(title);
        quantity = Validator.validateQuantityInput(scanner);
        model.setQuantity(quantity);

        title = "Please, input price: ";
        System.out.println(title);
        price = Validator.validatePriceInput(scanner);
        model.setPrice(price);
        scanner.close();
    }

    public void getOutput(String output) {
        System.out.println(output);
    }
}
