package models;

import base.Fiscal;
import base.Income;

public class Product implements Fiscal, Income {

    private String name;
    private int quantity;
    private double price;
    private final double TAX_RATE = 0.05;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double incomeCalculation(int quantityOfGoods, double price) {
        return quantityOfGoods * price;
    }

    @Override
    public double taxCalculation(double income) {
        return income * TAX_RATE;
    }

    @Override
    public double incomeCalculation(double income) {
        return income - (income * TAX_RATE);
    }
}
