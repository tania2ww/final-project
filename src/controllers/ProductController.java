package controllers;

import models.Product;
import utils.Rounder;
import views.SalesView;

public class ProductController {

    private Product model;
    private SalesView view;

    public ProductController(Product model, SalesView view) {
        this.model = model;
        this.view = view;
    }

    public void runApp() {

        view.getInputs();
        String name = model.getName();
        double totalIncome = model.incomeCalculation(model.getQuantity(), model.getPrice());
        double totalIncomeAfterRounding = Rounder.getRounderValue(totalIncome);
        double netIncome = model.incomeCalculation(totalIncome);
        double netIncomeAfterRounding = Rounder.getRounderValue(netIncome);
        double tax = model.taxCalculation(totalIncome);
        double taxAfterRounding = Rounder.getRounderValue(tax);

        String output = "Name of goods: " + name + "\n" +
                "Total income is: " + totalIncomeAfterRounding + "\n" +
                "Tax is: " + taxAfterRounding + "\n" +
                "NetIncome is : " + netIncomeAfterRounding;

        view.getOutput(output);
    }
}
