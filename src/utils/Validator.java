package utils;

import controllers.NameOfGoods;
import java.util.Scanner;

public class Validator {

    public static String validateName(Scanner scanner) {
        String str = scanner.nextLine().trim();
        while (!"".equals(str)) {
            if (!isGoods(str)) {
                System.out.println("This value isn't correct. Try again: ");
            } else {
                return str;
            }
            str = scanner.nextLine().trim();
        }
        return str;
    }

    public static boolean isGoods(String str) {
        for (NameOfGoods name : NameOfGoods.values()) {
            if (str.equalsIgnoreCase(String.valueOf(name))) {
                return true;
            }
        }
        return false;
    }


    public static int validateQuantityInput(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            String str = scanner.nextLine().trim();
            System.out.printf("\"%s\" - This value isn't correct.\n", str);
            System.out.println("Please input a quantity!: ");
        }
        int quantity = scanner.nextInt();
        while (quantity <= 0) {
            System.out.println("Wrong value! Please input a quantity: ");
            while (!scanner.hasNextInt()) {
                String str = scanner.next().trim();
                System.out.printf("\"%s\" - This value isn't correct.\n", str);
                System.out.println(" Please input a quantity: ");
            }
            quantity = scanner.nextInt();
        }
        return quantity;
    }

    public static float validatePriceInput(Scanner scanner) {
        while (!scanner.hasNextFloat()) {
            String str = scanner.nextLine().trim();
            System.out.printf("\"%s\" - it isn't a digit!\n", str);
            System.out.println("Please input a price!: ");
        }
        float price = scanner.nextFloat();
        while (price <= 0) {
            System.out.println("Wrong value!Please input a price!: ");
            while (!scanner.hasNextInt()) {
                String str = scanner.next().trim();
                System.out.printf("\"%s\" - it isn't a digit!\n", str);
                System.out.println(" Please input a price: ");
            }
            price = scanner.nextFloat();
        }
        return price;
    }
}

