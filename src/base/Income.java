package base;

public interface Income {

    double incomeCalculation(int quantityOfGoods, double price);

    double incomeCalculation(double income);
}
